.. How to download and install FEniCS projects.

.. _download:

.. include:: icons.rst

########
Download
########

.. _binary_packages:

*********************************
FEniCS versions and release notes
*********************************

The latest stable release of **FEniCS version 1.4.0** released
2014-06-02. For more information about FEniCS releases and changes,
take a look at :ref:`release_notes`. Note that binary packages will
appear a few days after the release of a new version.

***************
Binary packages
***************

This is the main FEniCS download, suitable for most users. It includes
everything needed to get you started with FEniCS.

.. raw:: html
    :file: index.inc

===========================
Contributed binary packages
===========================

For information on user contributed binary packages, see
:ref:`contributed_packages`.

************************
Installation from source
************************

If the binaries do not apply to you, or you want to install directly
from source, try one of the two other possibilities:

* :ref:`Automated installation using Dorsal <installation_using_dorsal>`
* :ref:`Manual installation from source <installation_from_source>`

*************************
Nightly snapshot releases
*************************

Every night, FEniCS snapshot releases are automatically generated for
Ubuntu and Mac OS X. They are made available at our :ref:`snapshots page
<snapshot_releases>`.

*******************
Development version
*******************

For information on accessing the development repositories, see
:ref:`developers`.

***************
Data and meshes
***************

A collection of meshes for free use with FEniCS is available
:ref:`here <data>`.

***************
Troubleshooting
***************

Visit the :ref:`troubleshooting page <troubleshooting>` if you have
problems installing FEniCS.

.. toctree::
   :hidden:
   :glob:

   *
