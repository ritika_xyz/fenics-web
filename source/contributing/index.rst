.. Developer information.

.. _developers:

############
Contributing
############

FEniCS is a free/open source project and everyone is invited to
contribute. This page contains information for prospective FEniCS
developers, including an overview of the organization of the FEniCS
project, how to write code and documentation, and how to use tools
like Bitbucket and Git.

************
Organization
************

FEniCS is organized as a collection of interoperable components that
together form the FEniCS Project. Each component is developed by one
or more authors. This means that each component can be developed at
its own pace. At the same time, we strive to make periodic and
coordinated releases of all components to ensure interoperability
between the components.

The FEniCS Project uses `Bitbucket <https://bitbucket.org>`_ as its
main development platform. All FEniCS projects are collected under
the `FEniCS Project on Bitbucket
<https://bitbucket.org/fenics-project>`_.

***************************
Instructions for developers
***************************

The following pages list important instructions for FEniCS developers.

.. toctree::
   :maxdepth: 1

   Obtaining the source code <obtaining_code>
   Writing code <writing_code>
   Contributing code <contributing_code>
