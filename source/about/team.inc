<div id="main">
  <div id="container" class="feature">
    <div id="content">
      <div id="sub-feature">

      	<div class="front-block block">
  	  <h2>Martin Sandve Alnæs</h2>
          <div class="avatar mugshot">
	    <img alt='Martin Sandve Alnæs'
		 src='/_static/images/mugshots/alnaes.jpg' />
	  </div>

	  <p>Martin is a postdoctoral fellow at <a
	  href="http://simula.no/">Simula Research Laboratory</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2006,
          is the main developer of UFL and SFC, and one of the
          developers of UFC, Instant, SyFi and DOLFIN.</p>

	  <p>You can view all Martin&#8217;s contributions on <a
          href="https://launchpad.net/~martinal">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Johan Hake</h2>
          <div class="avatar mugshot">
	    <img alt='Johan Hake'
		 src='/_static/images/mugshots/hake.jpg' />
	  </div>

	  <p>Johan is a postdoctoral fellow at <a
	  href="http://simula.no/">Simula Research Laboratory</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2007,
          is a maintainer of the Python interface of DOLFIN, and is
          a developer of Instant, UFC and FFC.</p>

	  <p>You can view all Johans&#8217;s contributions on <a
          href="https://launchpad.net/~johan-hake">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Johan Hoffman</h2>
          <div class="avatar mugshot">
	    <img alt='Johan Hoffman'
		 src='/_static/images/mugshots/hoffman.jpg' />
	  </div>

	  <p>Johan is a professor at the <a
	  href="http://www.kth.se/">KTH Royal Institute of Technology</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2003
          and is a developer of DOLFIN and Unicorn.</p>

	  <p>You can view all Johans&#8217;s contributions on <a
          href="https://launchpad.net/~jhoffman">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Johan Jansson</h2>
          <div class="avatar mugshot">
	    <img alt='Johan Jansson'
		 src='/_static/images/mugshots/jansson.jpg' />
	  </div>

	  <p>Johan is a senior researcher at the <a
	  href="http://www.kth.se/">KTH Royal Institute of
	  Technology</a>.</p>

	  <p>He has been involved with the FEniCS Project since 2004
	    and is a developer of DOLFIN and Unicorn.</p>

	  <p>You can view all Johan&#8217;s contributions
	  on <a href="https://launchpad.net/~jjan">his Launchpad
	  page.</a>

        </div>

      	<div class="front-block block">
  	  <h2>Robert C. Kirby</h2>
          <div class="avatar mugshot">
	    <img alt='Robert C. Kirby'
		 src='/_static/images/mugshots/kirby.jpg' />
	  </div>

          <p>Robert is an associate professor at
          <a href="http://www.baylor.edu/">Baylor University</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2003
          and is the main developer of FIAT.</p>

	  <p>You can view all Robert&#8217;s contributions on <a
          href="https://launchpad.net/~robert-c-kirby">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2><a href="http://www.math.chalmers.se/~logg/">Anders Logg</a></h2>
          <div class="avatar mugshot">
	    <img alt='Anders Logg'
		 src='/_static/images/mugshots/logg.jpg' />
	  </div>

          <p>Anders is a professor at the
          <a href="http://www.chalmers.se/en/departments/math/Pages/default.aspx">Department of Mathematical Sciences</a> at
          <a href="http://www.chalmers.se/en/">Chalmers University of Technology</a>.

          <p>He has been involved with the FEniCS project since 2003
          and is a developer and maintainer of DOLFIN, FFC, UFC, UFL,
          CBC.Solve, Exterior and the FEniCS Project web page.</p>

          <p>You can view all Anders' contributions on <a
          href="https://bitbucket.org/logg">his Bitbucket
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Kent-Andre Mardal</h2>
          <div class="avatar mugshot">
	    <img alt='Kent-Andre Mardal'
		 src='/_static/images/mugshots/mardal.jpg' />
	  </div>

	  <p>Kent is a senior research scientist at <a
	  href="http://simula.no/">Simula Research Laboratory</a> and
	  an adjunct associate professor at the <a
	  href="http://www.uio.no/">University of Oslo</a>.</p>

          <p>He has been involved with the FEniCS Project since 2006
	  and is a developer of DOLFIN, Instant, SyFi, Exterior,
	  CBC.Block and UFC.</p>

          <p>You can view all Kent&#8217;s contributions on <a
          href="https://launchpad.net/~kent-and">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Andre Massing</h2>
          <div class="avatar mugshot">
	    <img alt='Andre Massing'
		 src='/_static/images/mugshots/massing.jpg' />
	  </div>

	  <p>Andre is a Ph.D. student at <a
	  href="http://simula.no/">Simula Research Laboratory</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2009,
          has developed the mesh intersection functionality in DOLFIN
          and is the main developer of DOLFIN-OLM.</p>

	  <p>You can view all Andre&#8217;s contributions on <a
          href="https://launchpad.net/~massing">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Harish Narayanan</h2>
          <div class="avatar mugshot">
	    <img alt='Harish Narayanan'
		 src='/_static/images/mugshots/harish.jpg' />
	  </div>

	  <p>Harish is a postdoctoral fellow at <a
	  href="http://simula.no/">Simula Research Laboratory</a>.
          </p>

	  <p>He has been involved with the FEniCS project since 2007
	  and is the primary developer of Dorsal (the FEniCS build
	  system), the FEniCS project web site and CBC.Twist (a
	  general solid mechanics solver) distributed as part of
	  CBC.Solve.</p>

	  <p>You can view all Harish&#8217;s contributions on <a
          href="https://launchpad.net/~hnarayanan">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Johannes Ring</h2>
          <div class="avatar mugshot">
	    <img alt='Johannes Ring'
		 src='/_static/images/mugshots/ring.jpg' />
	  </div>

	  <p>Johannes is a research engineer at <a
	  href="http://simula.no/">Simula Research Laboratory</a>.</p>

          <p>He has been involved with the FEniCS Project since 2008,
          mostly as a build engineer.</p>

	  <p>You can view all Johannes' contributions on <a
          href="https://launchpad.net/~johannr">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Marie Rognes</h2>
          <div class="avatar mugshot">
	    <img alt='Marie Rognes'
		 src='/_static/images/mugshots/rognes.jpg' />
	  </div>

	  <p>Marie is a postdoctoral fellow at <a
	  href="http://simula.no/">Simula Research Laboratory</a>.</p>

	  <p>She has been involed with the FEniCS Project since 2007,
	  is a developer of FIAT, FFC, UFL, DOLFIN
	  and the lead developer of ASCoT.</p>

	  <p>You can view all Marie&#8217;s contributions on <a
          href="https://launchpad.net/~meg-simula">her Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Kristoffer Selim</h2>
          <div class="avatar mugshot">
	    <img alt='Kristoffer Selim'
		 src='/_static/images/mugshots/selim.jpg' />
	  </div>

	  <p>Kristoffer is a postdoctoral fellow at the <a
	  href="http://www.ffi.no/">Norwegian Defence Research Establishment</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2008 and
          is one of the developers of the fluid-structure interaction
          solver CBC.Swing distributed as part of CBC.Solve.</p>

	  <p>You can view all Kristoffer&#8217;s contributions on <a
          href="https://launchpad.net/~selim-simula">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Ridgway Scott</h2>
          <div class="avatar mugshot">
	    <img alt='Ridgway Scott'
		 src='/_static/images/mugshots/scott.jpg' />
	  </div>

	  <p>Ridgway is a professor at the <a
	  href="http://cs.uchicago.edu/">University of Chicago</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2003.</p>

	  <p>You can view all Ridg&#8217;s contributions on <a
          href="https://launchpad.net/~ridg">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Garth N. Wells</h2>
          <div class="avatar mugshot">
	    <img alt='Garth N. Wells'
		 src='/_static/images/mugshots/wells.jpg' />
	  </div>

          <p>Garth is the Hibbit Reader in Solid Mechanics at the
          <a href="http://www.eng.cam.ac.uk/">Department of Engineering</a>,
          University of Cambridge.</p>

          <p>He has been involved with the FEniCS project since 2005
          and is a developer and maintainer of DOLFIN, FFC, UFC and
          UFL.</p>

          <p>You can view all Garth's contributions on <a
          href="https://launchpad.net/~garth-wells">his Launchpad
          page.</a></p>

        </div>

      	<div class="front-block block">
  	  <h2>Kristian Breum Ølgaard</h2>
          <div class="avatar mugshot">
	    <img alt='Kristian Breum Ølgaard'
		 src='/_static/images/mugshots/oelgaard.jpg' />
	  </div>

	  <p>Kristian is a research assistant at <a
	  href="http://en.esbjerg.aau.dk/">Aalborg University</a>.
          </p>

          <p>He has been involved with the FEniCS Project since 2006,
          is one of the main developers of FFC and FEniCS Plasticity, and a
          developer of UFL and DOLFIN.</p>

	  <p>You can view all Kristian&#8217;s contributions on <a
          href="https://launchpad.net/~k.b.oelgaard">his Launchpad
          page.</a></p>

        </div>

      </div><!-- #sub-feature -->
    </div><!-- #content -->
  </div><!-- #container .feature -->
</div><!-- #main -->
